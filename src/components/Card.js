import React from 'react';

const Card = (props) => {
    const {imageUrl, description, duration} = props;
    return(
        <div className="col-sm-4">
            <div className="card sm-4 shadow-sm add-margin">
                <img src={imageUrl} alt="car" style={{width: "100%"}, {height: "225px"}}></img>
                <div className="card-body">
                    <p className="card-text">{description}</p>
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="btn-group">
                            <button type="button" className="btn btn-sm btn-outline-secondary">View</button>
                            <button type="button" className="btn btn-sm btn-outline-secondary">Edit</button>
                        </div>
                        <small className="text-muted">{duration}</small>
                    </div>
                </div>
            </div>
        </div>
    );
}

export { Card };