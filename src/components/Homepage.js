import React, { Component } from 'react';
import { Card } from '../components/Card';
import '../App.css';

var key;
var values;
function importAll(r) {
    return r.keys().map(r);
}
const images = importAll(require.context('../assets/images',false, /\.jpg/));

for(key in images) {
    if(images.hasOwnProperty(key)) {}
}

class Homepage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {
                    imageUrl: images[0]['default'],
                    description: 'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.',
                    duration: '9 mins'
                },
                {
                    imageUrl: images[1]['default'],
                    description: 'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.',
                    duration: '9 mins'
                },
                {
                    imageUrl: images[2]['default'],
                    description: 'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.',
                    duration: '9 mins'
                },
                {
                    imageUrl: images[3]['default'],
                    description: 'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.',
                    duration: '9 mins'
                },
                {
                    imageUrl: images[4]['default'],
                    description: 'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.',
                    duration: '9 mins'
                },
                {
                    imageUrl: images[5]['default'],
                    description: 'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.',
                    duration: '9 mins'
                },
                {
                    imageUrl: images[6]['default'],
                    description: 'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.',
                    duration: '9 mins'
                },
                {
                    imageUrl: images[7]['default'],
                    description: 'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.',
                    duration: '9 mins'
                },
                {
                    imageUrl: images[8]['default'],
                    description: 'This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.',
                    duration: '9 mins'
                }
            ]
       };
    }

    cardRenders = e => {
        values = this.state.data;
        if(values.length !== 0) {
            return values.map(item => 
                <Card imageUrl={item.imageUrl} description={item.description} duration={item.duration}></Card>
            );
        } 
    }

    render() {
        return (
            <div>
                <header>
                    <div className="bg-dark collapse" id="navbarHeader">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-8 col-md-7 py-4">
                                    <h4 className="text-white">About</h4>
                                    <p className="text-muted">Add some information about the album below, the author, or any other background context. Make it a few sentences long so folks can pick up some informative tidbits. Then, link them off to some social networking sites or contact information.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="navbar navbar-dark bg-dark shadow-sm">
                        <div className="container d-flex justify-content-between">
                            <a href={this.state.data} className="navbar-brand d-flex align-items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" aria-hidden="true" className="mr-2" viewBox="0 0 24 24" focusable="false"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>
                                <strong>Homework-01</strong>
                            </a>
                            <button className="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                        </div>
                    </div>
                </header>
                <main role="main">
                    <section className="jumbotron text-center">
                        <div className="container">
                            <h1>Homework Album</h1>
                            <p className="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it entirely.</p>
                            <p>
                                <a href={this.state.data} className="btn btn-primary my-2">Main call to action</a>
                                {' '}
                                <a href={this.state.data} className="btn btn-secondary my-2">Secondary action</a>
                            </p>
                        </div>
                    </section>
                </main>
                <div className="album py-5 bg-light">
                    <div className="container">
                        <div className="row">
                            {this.cardRenders()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Homepage